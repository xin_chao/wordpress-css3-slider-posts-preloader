<?php
/*
*  this is a template that should be included in your
*  template page.php or header.php or wherever it fits.
*/
?>
<!-- //BEGIN// CSS Slider v2 -->
<?php
$meta_query_args = array(
	'meta_key'   => 'rw_sticky_yes',
	'meta_value' => 'yes',
	'post_type' => array('post', 'page', 'ehevent', 'ehproject'),
	'orderby' => 'date'
);
$the_query = new WP_Query( $meta_query_args );
$the_data_array = [];
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
		$the_query->the_post();
		$imgsrc =  get_the_post_thumbnail_url( NULL, 'large'  ); // 'slider-hero'
		$to_push = [];
		  if ( get_the_post_thumbnail_url( NULL, 'large' ) == false ) {
				$imgsrc = get_template_directory_uri() . "/cssslider/v2/ehda_slider_default.jpg";
			}
		$to_push = [get_the_title(), $imgsrc, get_the_permalink(), rwmb_meta('rw_sticky_cta')  ];
		array_push($the_data_array,$to_push);
	}
}
$labels = count($the_data_array);
if ($labels>6){$labels=6;} // cap at 6
?>
<style media="screen">
.preload-images {
background: url(<?php echo get_template_directory_uri() . "/cssslider/v2/ehda_slider_default.jpg"; ?>) no-repeat -9999px -9999px;
background:
<?php foreach ($the_data_array as $item) { ?>
 url(<?php echo $item[1] ?>) no-repeat -9999px -9999px,
<?php } ?>
}
</style>
<div class="preload-images"></div>
<ul class="slides">
<?php for ($i=0; $i < 6; $i++) { ?>
	<?php
	if ($i+1<$labels) { $next = $i+2;}
	else { $next = 1; }
	if ($i>0){ $prev = $i;}
	else { $prev = 6;}
	?>
	<input type="radio" name="radio-btn" id="img-<?php echo $i+1 ?>" <?php if ($i==0){ echo " checked"; } ?> />
	<li class="slide-container">
			<div class="slide">
				<figure>
					<div class="slide-figure">
						<h4><a target="_blank" href="<?php echo $the_data_array[$i][2] ?>"><?php echo $the_data_array[$i][0] ?></a></h4>
						<a class="button" target="_blank" href="<?php echo $the_data_array[$i][2] ?>"><?php echo $the_data_array[$i][3] ?></a>
					</div>
				</figure>
				<img src="<?php echo $the_data_array[$i][1] ?>" />
			</div>
	<div class="nav">
		<label for="img-<?php echo $prev ?>" class="prev">&#x2039;</label>
		<label for="img-<?php echo $next ?>" class="next">&#x203a;</label>
	</div>
	</li>
<?php } ?>
<li class="nav-dots">
<?php
for ($i=1; $i < $labels+1; $i++) {  ?>
	<label for="img-<?php echo $i ?>" class="nav-dot" id="img-dot-<?php echo $i ?>"></label>
<?php } ?>
</li>
</ul>
<div class="clear"></div>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cssslider/v2/style.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
<!-- //END// CSS Slider v2 -->
