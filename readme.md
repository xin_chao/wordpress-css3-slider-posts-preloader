## CSS3 Slider for Wordpress Posts 

**Dependencies**

* Needs custom metaboxes. These can be made from scratch or with the help of a plugin from metabox.io.
* This uses custom post types ehevent and ehproject. Remove this from the metabox setup and the WP_Query or swap for your custom post types.

**Into your funcions.php:**

```
add_filter( 'rwmb_meta_boxes', 'tooltip_register_meta_boxes' );
function tooltip_register_meta_boxes( $meta_boxes ) 
{
// CTA sticky posts
    $meta_boxes[] = array(
        'title'    => 'Slider Button CTA',
        'pages'    => array( 'post', 'page', 'ehevent', 'ehproject' ),
        'fields' => array(
            array(
                'name' => 'Use with sticky posts to customize the button \'read more\' text. Default: Read more.',
                'id'   => $prefix . 'sticky_cta',
                'type' => 'text',
                'std'   => 'Read more',
                'size' => 60,
                'clone' => false,
            ),
            array(
              'name' => 'Use this post for slider (max 6 posts)',
              'id'  => $prefix . 'sticky_yes',
              'type' => 'radio',
              'options' => array(
      					'yes' => esc_html__( 'Yes, use this for slider', 'yes_' ),
                'no' => esc_html__( 'Not use or remove this from slider', 'no_' ),
      				 ),
              'clone' => false,
            )
        )
    );
}
```


.
